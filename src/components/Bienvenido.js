import { Typography } from '@material-ui/core'
import React, { Component } from 'react'

export class Bienvenido extends Component {
    render() {
        return (
            <div>
                <Typography variant="h6" color="inherit">
                Bienvenido querido usuario al Banco Desafío :)
                </Typography>
            </div>
        )
    }
}

export default Bienvenido
