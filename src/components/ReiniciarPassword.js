import { Button, TextField } from '@material-ui/core'
import axios from 'axios';
import React, { Component } from 'react'
import {  toast } from 'react-toastify';
import { SERVER_REINICIO } from '../constants';

export default class ReiniciarPassword extends Component {
    state = {
        email: ""
    }
    cambiarPassword = async () => {
        const data = new FormData();

        data.append("email", this.state.email)

        await this.postReiniciarPasssword(data).then((resp) => {
            console.log(resp.data)
            const respuestaText = resp.data;
            if (respuestaText !== "Email invalido") {
                console.log("token de reinicio: " + respuestaText);
                window.location.href = "/login";
                toast.success("Mail enviado a su correo electronico", toast.POSITION.TOP_RIGHT)
            }
            else {
                toast.warn("Email incorrectos.!", {
                    position: toast.POSITION.BOTTOM_LEFT
                });
            }
        });
    }

    handleChange = (event)  => {
        if(this.state.email === null) {
        toast.warn("Email incorrectos.!", {
            position: toast.POSITION.BOTTOM_LEFT
            })
        }
        if(event.target.value != null)
            this.setState({"email": event.target.value});
    }


    postReiniciarPasssword(data) {
        return axios.post(SERVER_REINICIO + 'forgot-password',
                data, {
                    headers: {
                    'Content-Type': 'multipart/form-data'
                    }
                })
            .then(res => {
                return res
               
            })
            .catch(err => console.error(err));
    }

    render() {
        return (
            <div>
                Para reiniciar su contraseña, favor ingresar su correo electronico:
                <br/>
                <TextField name="email" 
                label="email" type="email" onChange={(e) => this.handleChange(e)} /><br/> 
                <Button variant="outlined" color="primary" 
                onClick={this.cambiarPassword}>
                Reiniciar contraseña
                </Button>
            </div>
        )
    }
}
