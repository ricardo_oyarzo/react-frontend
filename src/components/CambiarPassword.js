import { Button, TextField } from '@material-ui/core';
import React, { Component } from 'react';
import './CambiarPassword.css';
import {  toast } from 'react-toastify';
import { SERVER_REINICIO } from '../constants';
import axios from 'axios';

export default class CambiarPassword extends Component {
    state = {
        token: this.props.match.params.token,
        password: "",
        repetirPassword: "",
        password_error_text: "Las contraseñas ingresadas deben ser iguales",
        passwordValida: true
    }

    handleChangePassword = (e) => {

        const {name, value} = e.target;

        this.setState({[name]: value}, () => {
            // console.log(this.state.password.trim(),this.state.repetirPassword.trim(),this.state.password.trim() === this.state.repetirPassword.trim());
            if(this.state.password.trim() === this.state.repetirPassword.trim()) {
                this.setState({"passwordValida": true});
            } else {
                this.setState({"passwordValida": false});
            }
        });
        
    }

    cambiarPassword = async () => {
            const data = new FormData();
    
            data.append("token", this.state.token)
            data.append("password", this.state.password)
    
            await this.postCambiarPasssword(data).then((resp) => {
                const respuestaText = resp.data;
                if (respuestaText !== "Token invalido") {
                    window.location.href = "/login";
                    toast.success("Contraseña cambiada exitosamente", toast.POSITION.TOP_RIGHT)
                } else {
                    toast.warn("Token invalido.!", {
                        position: toast.POSITION.TOP_RIGHT
                    });
                }
            });
    }


    postCambiarPasssword(data) {
        return axios.post(SERVER_REINICIO + 'reset-password',
                data, {
                    headers: {
                    'Content-Type': 'multipart/form-data'
                    }
                })
            .then(res => {
                return res
               
            })
            .catch(err => console.error(err));
    }

    render() {
        return (
            <div>
                <span className="introduccion">Ingrese su nueva contraseña:</span>
                <br/>
                <TextField name="password" 
                    label="Password" 
                    onChange={(e) => this.handleChangePassword(e)} 
                    type="password"
                    error={!this.state.passwordValida}/>
                <br/> 
                <TextField name="repetirPassword" 
                    label="Repetir Password"
                    type="password" 
                    onKeyUpCapture={(e) => this.handleChangePassword(e)} 
                    error={!this.state.passwordValida}/>
                <br/> 
                <Button variant="outlined" color="primary" 
                onClick={this.cambiarPassword} disabled={(!this.state.passwordValida || this.state.password === "")}>
                Cambiar contraseña
                </Button>
            </div>
        )
    }
}
