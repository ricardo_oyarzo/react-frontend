import React, { useState } from 'react';
import {SERVER_URL} from '../constants.js';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Bienvenido from './Bienvenido.js';
import { Link } from 'react-router-dom';
import './Login.css';
import axios from 'axios';

const Login = () => {
  const [user, setUser] = useState({username: '', password: ''})
  const [isAuthenticated, setAuth] = useState(false);
 
  const handleChange = (event) => {
    setUser({...user, [event.target.name] : event.target.value})
  }

  const login = async () => {
    const data = new FormData();
    data.append("username", user.username)
    data.append("password", user.password)

    await postLogin(data)
    .then(res => {
      // Si el backend responde con el token en la cabecera
      // este se almacena en sessionStorage
      if(res !== undefined) {
        const jwtToken = res.headers["authorization"];
        if (jwtToken !== null) {
          sessionStorage.setItem("jwt", jwtToken);
          setAuth(true);
        }
      }
    });
  }  

  if (isAuthenticated === true) {
    return (<Bienvenido />)
  }
  else {
    return (
        <div>
            <TextField name="username" 
            label="Nombre de usuario" onChange={handleChange} /><br/> 
            <TextField type="password" name="password" 
            label="Contraseña" onChange={handleChange} /><br/><br/> 
            <Button variant="outlined" color="primary" 
            onClick={login}>
            Iniciar Sesión
            </Button>

            <br />
            <Link
            to="/reiniciarpassword"
            variant="body2"
            onClick={() => {
                console.info("I'm a button.");
            }}
            >
            ¿Ha olvidado su contraseña?.
            </Link>
            
        </div>
    );
  }
}

export default Login;

async function postLogin(data) {
  return axios
    .post(SERVER_URL + 'login', data, {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    }).then(resp => {return resp;})
    .catch(err => {
      console.log(err);
      toast.error("Nombre de usuario y contraseña incorrectos.!", {
        position: toast.POSITION.TOP_RIGHT
      }) 
    });
}
