import React from 'react';
import './App.css';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Login from './components/Login';
import ReiniciarPassword from './components/ReiniciarPassword';
import { Route, BrowserRouter, Switch } from 'react-router-dom';
import ProtectedRoute from './ProtectedRoute';
import { ToastContainer } from 'react-toastify';
import { Container } from '@material-ui/core';
import CambiarPassword from './components/CambiarPassword';

function App() {
 
  return (
      <div className="App">
        
        <BrowserRouter>
        <AppBar position="static" color="default">
          <Toolbar>
            <Typography variant="h6" color="inherit">
              Banco Desafío
            </Typography>            
          </Toolbar>
        </AppBar>
        <Container fixed>
          <Switch>
                  <Route path="/login" component={Login} />
                  <Route path="/reiniciarpassword" component={ReiniciarPassword} />
                  <Route path="/cambiarPassword/:token" component={CambiarPassword} />
                  <ProtectedRoute
                    path="/"  />
                  {<Route path="/*" render={() => <h1>Página no encontrada</h1>} />}
          </Switch>
        </Container>
        </BrowserRouter>
        <ToastContainer autoClose={5000} />
      </div>
  );
}

export default App;