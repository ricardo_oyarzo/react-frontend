import React from 'react'
import { Redirect, Route } from 'react-router-dom';

const ProtectedRoute = ({ component: Component, ...rest }) => {
    const auth = sessionStorage.getItem("jwt") !== null;
    
    return(       
            <Route {...rest} render={(props) => (
                    auth === true
                        ? <Component {...props} />
                        : <Redirect to='/login' />
                )} />
            );
}

export default ProtectedRoute;
